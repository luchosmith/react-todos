
const config        = require('./server/config/config')
const app           = require('./server/server')
const debug         = require('debug')('cli-app:server')
const http          = require('http')

const mongoose      = require('mongoose')
mongoose.connect(config.mongodb.dsn)
  .then(() => {
    console.log('Connected to mongodb')
  })

const port = config.port
const server = http.createServer(app)

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
  console.log('Listening on', bind);
  console.log('running out of : ', __dirname);
}
