import { createStore, applyMiddleware, compose } from 'redux';
import reduxPromise from 'redux-promise';
import mainReducer from './reducers';

const configureStore = () => {

  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    return createStore(
        mainReducer,
        {
          user: {
              name: 'Lucho',
              email: 'lucho.smith@gmail.com'
          },
          todos: [{
              _id: 1,
              text: 'foo'
          },{
              _id: 2,
              text: 'bar'
          }]
        },
        composeEnhancers(
            applyMiddleware(reduxPromise)
        )
    );
}

export default configureStore;

