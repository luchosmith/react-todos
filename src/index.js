import React, { Component } from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"
import configureStore from './store'
import * as actions from './actions'
import { BrowserRouter as Router, Route, NavLink } from 'react-router-dom'

import Nav from './components/nav'
import Main from './components/main'
import Todo from './components/todo'
import Process from './components/process'

class Root extends Component {

    constructor() {
        super();
        this.store = configureStore();
        this.state = this.store.getState();
        // if (!this.state.user.name) {
        //     this.store.dispatch(actions.getUser())
        // }
    }

    render() {
        return (
            <Provider store={this.store}>
                <Router>
                    <div>
                        <Nav user={this.state.user} />
                        <Route exact path="/" component={Todo} />
                        <Route exact path="/inbox" component={Todo} />
                        <Route exact path="/process" component={Process} />
                    </div>
                </Router>
            </Provider>
        )
    }
}

ReactDOM.render((<Root />), document.getElementById('react-root'))
