import React from "react"
import { connect } from "react-redux"
import * as actions from '../actions'

const Login = ({dispatch}) => {

    const handleSubmit = (event) => {
      event.preventDefault();
      dispatch(actions.loginUser({
          email: event.target.useremail.value
      }));
    }
  
    return (
        <div>
            <h4>Login</h4>
            <form onSubmit={handleSubmit}>
                <label>
                    email:
                    <input name="useremail" type="email" />
                </label>
                <input type="submit" value="Submit" />
            </form>
        </div>
    )
    
  }


export default connect()(Login)