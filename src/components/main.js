import React, { Component } from "react"
import { connect } from "react-redux"

import Nav from './nav'
import * as actions from '../actions'

const Main = ({ dispatch, todos, user, children }) => {

    if (!user.name) {
        //dispatch(actions.getUser())
    }

    return (
        <div className="o-container">
            <Nav user={user} />
            <div>{children}</div>
        </div>
    )
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user,
        todos: state.todos,
        children: ownProps.children
    }
}

export default connect(
    mapStateToProps
)(Main)
