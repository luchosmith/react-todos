import React, { Component } from "react"
import { connect } from "react-redux"

import Login from './login'
import NewTodo from './new-todo'
import NewUser from './new-user'
import Todos from './todos'

const Todo = ({ todos, user}) => {

    if(!user.name) {
        return (
            <div className="row">
                <Login />
                or
                <NewUser />
            </div>
        )
    } else {
        return (
            <div className="row">
                <NewTodo />
                <Todos todos={todos} />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
      user : state.user,
      todos: state.todos
    }
  }

export default connect(
    mapStateToProps
)(Todo)
