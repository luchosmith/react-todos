import React from "react"
import { connect } from "react-redux"
import * as actions from '../actions'

const NewUser = ({dispatch}) => {

    const handleSubmit = (event) => {
        console.log(event.target.username.value, event.target.useremail.value)
        dispatch(actions.saveUser({
            username: event.target.username.value,
            useremail: event.target.useremail.value
        }));
        event.preventDefault();
    }
  
    return (
        <form onSubmit={handleSubmit}>
            <label>
            Name:
            <input name="username" type="text" />
            </label>
            <label>
            Email:
            <input name="useremail" type="email" />
            </label>
            <input type="submit" value="Submit" />
        </form>
    )
    
  }

export default connect()(NewUser)