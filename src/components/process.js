import React, { Component } from "react"
import { connect } from "react-redux"

import Todos from './todos'

const Process = ({ todos, user}) => {

    return (
        <div className="row">
            <h2>Process</h2>
            <Todos todos={todos} />
        </div>
    )
}

const mapStateToProps = state => {
    return {
      user : state.user,
      todos: state.todos
    }
  }

export default connect(
    mapStateToProps
)(Process)
