import React from 'react'

const Todos = ({todos=[]}) => {

 return (<ul className="todos">
      {todos.map(todo => {
          return (  
            <li key={todo._id} className="todo"> 
                <span>
                    {todo.text}
                </span>
            </li>
          )
      })}
    </ul>)
}

export default Todos