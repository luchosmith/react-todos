import React from 'react'
import { NavLink } from 'react-router-dom'

const Nav = ({user={}}) => {
    return(
        <nav className="navbar navbar-expand-md navbar-dark bg-dark">
            <a href="/" className="navbar-brand">{user.name || ''} Todos</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarText">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item"><NavLink className="nav-link" to="/inbox">Inbox</NavLink></li>
                    <li className="nav-item"><NavLink className="nav-link" to="/process">Process</NavLink></li>
                </ul>
            </div>
        </nav>)
}

export default Nav