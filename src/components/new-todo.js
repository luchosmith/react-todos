import React from "react"
import { connect } from "react-redux"
import * as actions from '../actions'

const NewTodo = ({dispatch}) => {

    const handleSubmit = (event) => {
      event.preventDefault();
      dispatch(actions.saveTodo({
          text: event.target.todotext.value
      }));
    }
  
    return (
        <div>
            <h4>New Todo</h4>
            <form onSubmit={handleSubmit}>
                <label>
                    description:
                    <input name="todotext" type="text" />
                </label>
                <input type="submit" value="Submit" />
            </form>
        </div>
    )
    
  }


export default connect()(NewTodo)