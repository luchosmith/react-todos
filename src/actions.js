
export const loginUser = (email) => {
    return fetch(`/login`, {
        method: 'POST',
        body: JSON.stringify(email),
        credentials: 'same-origin',
        headers: {
            'content-type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(data => {
            return ({
                type: 'RECEIVE_USER',
                user: data.user,
                todos: data.todos
            })
        })
}

export const getUser = () => {
    return fetch(`/user`, {
        credentials: 'same-origin'
    })
    .then(response => response.json())
    .then(data => {
        return ({
            type: 'RECEIVE_USER',
            user: data.user,
            todos: data.todos
        })
    });
}

export const saveUser = (user) => {
    return fetch(`/user`, {
        method: 'POST',
        body: JSON.stringify(user),
        credentials: 'same-origin',
        headers: {
            'content-type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(data => {
            return ({
                type: 'SAVED_USER',
                user: data.user
            })
        })
}


//TODO: pass in user (or handle that on the server?)
export const fetchTodos = () => {
    return fetch(`/todos`, { credentials: 'same-origin' })
        .then(response => response.json())
        .then(todos => {
            return ({
                type: 'RECEIVE_TODOS',
                todos
            })
        });
}

export const saveTodo = (todo) => {
    return fetch(`/todos`, {
        method: 'POST',
        body: JSON.stringify(todo),
        credentials: 'same-origin',
        headers: {
            'content-type': 'application/json'
        }
    })
        .then(response => response.json())
        .then(data => {
            return ({
                type: 'RECEIVE_TODO',
                todo: data.todo
            })
        })
}





