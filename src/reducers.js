const mainReducer = (state, action) => {
    switch(action.type) {
        case 'RECEIVE_USER': {
            return Object.assign({}, state, {user: action.user, todos: action.todos})
        }
        case 'SAVED_USER': {
            return Object.assign({}, state, {user: action.user});
        }
        // TODO: use spread operator
        case 'RECEIVE_TODO': {
            return Object.assign({}, state, {todos: state.todos.concat([action.todo])})
        }
        case 'RECEIVE_TODOS': {
            return Object.assign({}, state, {todos: action.todos})
        }
        default:
          return state;
    }
}

export default mainReducer;
