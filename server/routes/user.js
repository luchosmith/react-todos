const mongoose = require('mongoose')
const User = require('../../models/user')
const todo = require('./todo')

exports.createUser = (req, res) => {
    User.create({
        name: req.body.username,
        email: req.body.useremail,
        lastLogin : Date.now()
    }, ( err, user ) => {
        if (!err) {
            req.session.userid = user._id
            res.json({user: user})
        } else {
            console.log(err)
        }
    })
}

// TODO split this
exports.loginUser = (req, res) => {
    if(req.body.email) {
        User.findOne({email: req.body.email}, (err, user) => {
            if (!err) {
                req.session.user = user
                console.log('saved user to session, I think: ', JSON.stringify(req.session.user))
                todo.getTodos(req, res)
            } else {
                console.log(err)
            }
        })
    }
}

exports.getUser = (req, res) => {
    if(req.session.user) {
        console.log('found user in session')
        todo.getTodos(req, res)
    } else {
        res.json({user:{},todos:[]})
    }
}
