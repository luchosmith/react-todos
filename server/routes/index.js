const todo = require('./todo')
const user = require('./user')

module.exports = (app) => {
    app.use((req, res, next) => {
        console.log('middleware', JSON.stringify(req.session))
        if(req.session.user) {
            console.log(`user in session... ${req.session.user.name}`)
        }
        next()
    })
    app.post('/user', user.createUser)
    app.post('/login', user.loginUser)
    app.get('/user', user.getUser)
    app.get('/todos', todo.getTodos)
    app.post('/todos', todo.createTodo)
}