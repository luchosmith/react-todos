const mongoose = require('mongoose')
const Todo = require('../../models/todo')

exports.getTodos = (req, res) => {
    console.log('getting todos...')
    Todo.find({userId: req.session.user._id}, (err,todos) => {
        if (!err) {
            res.json({user: req.session.user, todos: todos || []})
        } else {
            console.log(err)
        }
    })
}

exports.createTodo = (req, res) => {
    Todo.create({
        userId: req.session.user._id,
        text: req.body.text,
        due_date: req.body.due_date,
        tags: req.body.tags ? req.body.tags.split(' ') : [],
        context: req.body.context,
        recurring: req.body.recurring,
        link: req.body.link
    }, ( err, todo ) => {
        if (!err) {
            console.log('Todo saved!', todo);
            res.json({todo: todo})
        }
    })
}

