const express       = require('express')
const config        = require('../config/config')
const path          = require('path')
const logger        = require('morgan')
const bodyParser    = require('body-parser')
const cookieParser  = require('cookie-parser')
const session       = require('express-session')


module.exports = (app) => {
    app.use(logger('dev'))
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(cookieParser())

    app.use(session({
        secret: 'keyboard cat',
        cookie: {},
        resave: true,
        saveUninitialized: true
    }))

    app.use(express.static(path.join(__dirname, '/../../public')))
    app.use(express.static(path.join(__dirname, '/../../node_modules')))
}


