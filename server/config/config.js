const config = {
    dev: 'development',
    test: 'test',
    prod: 'production',
    port: process.env.PORT || 5000,
    mongodb: {
        dsn: 'mongodb://localhost:27017/todos'
    }
}

process.env.NODE_ENV = process.env.NODE_ENV || config.dev
config.env = process.env.NODE_ENV


module.exports = config
