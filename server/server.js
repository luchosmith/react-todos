const express       = require('express')
const config        = require('./config/config')

var app = module.exports = express()

require('./middleware')(app)
require('./routes')(app)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  // var err = new Error(`Not found:  ${req.path}`);
  // err.status = 404;
  next({status: 404, message: `Not found:  ${req.path}`});
});

// development error handler
if (config.env === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    console.log('error', err);
  });
}

// production error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.end(err.message);
});
