const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    userId: mongoose.Schema.Types.ObjectId,
    name: {type:String, required: true},
    email: {type: String, unique: true, required: true},
    password: String,
    lastLogin: Date
})

module.exports = mongoose.model('users', userSchema)
