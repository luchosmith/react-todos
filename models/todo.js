const mongoose = require('mongoose')

const todoSchema = new mongoose.Schema({
    userId: mongoose.Schema.Types.ObjectId,
    text: String,
    due_date: String,
    created_timestamp: { type: Date, default: Date.now },
    tags: [],
    context: String,
    recurring: String,
    link: String,
})

module.exports = mongoose.model('todos', todoSchema)
